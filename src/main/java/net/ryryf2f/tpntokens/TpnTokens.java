package net.ryryf2f.tpntokens;

import net.luckperms.api.LuckPerms;
import net.milkbowl.vault.economy.Economy;
import net.ryryf2f.tpntokens.command.TokenCommandHandler;
import net.ryryf2f.tpntokens.econ.TpnEconomy;
import net.ryryf2f.tpntokens.io.flat.ConfigHandler;
import net.ryryf2f.tpntokens.io.sql.DatabaseManager;
import net.ryryf2f.tpntokens.listeners.InventoryListener;
import net.ryryf2f.tpntokens.listeners.PlayerJoinListener;
import net.ryryf2f.tpntokens.util.MessageHandler;
import net.ryryf2f.tpntokens.util.versioning.VersionChecker;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;


public class TpnTokens extends JavaPlugin
{


    private static TpnTokens instance;

    private static MessageHandler messageHandler;

    private static ConfigHandler configHandler;
    private static DatabaseManager databaseManager;

    //private static VersionChecker versionChecker;

    private static TpnEconomy econ = null;

    private static Economy vaultEcon;
    private static LuckPerms luckPerms;


    @Override
    public void onEnable()
    {

        instance = this;
        messageHandler = new MessageHandler(instance);


        configHandler = new ConfigHandler();
        configHandler.initConfigs();

        databaseManager = new DatabaseManager();

        //versionChecker = new VersionChecker();

        econ = new TpnEconomy();

        setupEconomy();
        setupLuckPerms();

        registerEvents();
        registerCommands();
    }

    @Override
    public void onDisable()
    {
        configHandler.shutdownConfigs();

        databaseManager = null;
        configHandler = null;
        instance = null;
    }

    private void registerCommands()
    {
        instance.getCommand("token").setExecutor(new TokenCommandHandler());
    }

    private void registerEvents()
    {
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), instance);
        getServer().getPluginManager().registerEvents(new InventoryListener(), instance);
    }

    public static TpnTokens getInstance()
    {
        return instance;
    }


    public static ConfigHandler getConfigHandler()
    {
        return configHandler;
    }

    public static TpnEconomy getEconomy()
    {
        return econ;
    }

    public static DatabaseManager getDatabaseManager()
    {
        return databaseManager;
    }

    public static Economy getVaultEconomy()
    {
        return vaultEcon;
    }

    public static LuckPerms getLuckPerms()
    {
        return luckPerms;
    }

    public static MessageHandler getMessageHandler()
    {
        return messageHandler;
    }

    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            vaultEcon = economyProvider.getProvider();
        }

        return (vaultEcon != null);
    }

    private boolean setupLuckPerms()
    {
        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (provider != null) {
            luckPerms = provider.getProvider();

        }
        return (luckPerms != null);
    }
}
