package net.ryryf2f.tpntokens.command;

import net.ryryf2f.tpntokens.TpnTokens;
import net.ryryf2f.tpntokens.frontpack.MainGui;
import net.ryryf2f.tpntokens.util.MessageHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class TokenCommandHandler implements CommandExecutor
{

    //token(s) give {player} {amount} -- gives tokens, admin    PLEASE NOTE THIS COMMAND EXECUTES DIRECTLY TO THE DB
    //token(s) take {player} {amount} -- takes tokens, admin    PLEASE NOTE THIS COMMAND EXECUTES DIRECTLY TO THE DB
    //token(s) balance {player} -- shows the balance of player, admin     PLEASE NOTE THIS COMMAND EXECUTES DIRECTLY TO THE DB
    //token(s) reload -- reloads database, admin
    //token(s) version -- shows verson, all
    //token(s) -- opens gui, all


    MessageHandler messageHandler = TpnTokens.getMessageHandler();


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (args.length == 0)
        {
            if (sender instanceof Player)
            {
                ((Player) sender).openInventory(new MainGui(((Player) sender).getUniqueId()).get());
                return true;
            }
        }else
        {
            if (sender instanceof ConsoleCommandSender)
            {
                return adminCommands(sender, command, label, args);
            }else
            {
                if (sender.hasPermission("tpntokens.admin"))
                {
                    return adminCommands(sender, command, label, args);
                }
            }
        }
        return infoMessage(sender);
    }

    public boolean adminCommands(CommandSender sender, Command command, String label, String[] args)
    {
        if (args.length == 1)
        {
            if (args[0].equalsIgnoreCase("version"))
            {

                sender.sendMessage(ChatColor.DARK_GRAY + "======================");
                sender.sendMessage(ChatColor.AQUA + "Plugin: " + ChatColor.DARK_GRAY + "TpnTokens");
                sender.sendMessage(ChatColor.AQUA + "Version: " + ChatColor.DARK_GRAY + TpnTokens.getInstance().getDescription().getVersion());
                sender.sendMessage(ChatColor.AQUA + "Developer: " + ChatColor.DARK_GRAY + "RyryF2F");
                sender.sendMessage(ChatColor.DARK_GRAY + "======================");

                return true;
            }else if (args[0].equalsIgnoreCase("reload"))
            {
                sender.sendMessage(messageHandler.formatMessage(ChatColor.RED + "This is not implemented yet due to syncing issues."));
                return true;
            }else if (args[0].equalsIgnoreCase("help"))
            {
                infoMessage(sender);
                return true;
            }else
            {
                sender.sendMessage(messageHandler.formatMessage(ChatColor.RED + "Please enter a valid command!"));
            }

        }else if (args.length == 2)
        {
            if (args[0].equalsIgnoreCase("balance"))
            {
                OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);

                if (player.hasPlayedBefore() && TpnTokens.getEconomy().hasAccount(player)) {
                    sender.sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "Players balance: " + TpnTokens.getEconomy().getBalance(player) + " Tokens."));
                }
            }else
            {
                sender.sendMessage(messageHandler.formatMessage(ChatColor.RED + "Please enter a valid command!"));
            }
        }else if (args.length == 3)
        {
            if (args[0].equalsIgnoreCase("give"))
            {
                OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);

                if (player.hasPlayedBefore() && TpnTokens.getEconomy().hasAccount(player))
                {
                    int change = Integer.parseInt(args[2]);
                    sender.sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "Attempting to give " + change + " to player: " + player.getName()));

                    TpnTokens.getEconomy().depositPlayer(player, change);

                    sender.sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "New balance on account appears to be: " + TpnTokens.getEconomy().getBalance(player)));
                    return true;
                }else
                {
                    sender.sendMessage(messageHandler.formatMessage(ChatColor.RED + "Either the given player has not played on the server, or their token account wasn't created."));
                    return true;
                }
            }else if (args[0].equalsIgnoreCase("take"))
            {
                OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);

                if (player.hasPlayedBefore() && TpnTokens.getEconomy().hasAccount(player))
                {
                    int change = Integer.parseInt(args[2]);
                    sender.sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "Attempting to take " + change + " from player: " + player.getName()));

                    if (TpnTokens.getEconomy().has(player, change))
                    {
                        TpnTokens.getEconomy().withdrawPlayer(player, change);
                        sender.sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + " New balance on account appears to be: " + TpnTokens.getEconomy().getBalance(player)));
                    }else
                    {
                        sender.sendMessage(ChatColor.RED + " Player does not have the required tokens!");
                        return true;
                    }
                }else
                {
                    sender.sendMessage(messageHandler.formatMessage(ChatColor.RED + "Either the given player has not played on the server, or their token account wasn't created."));
                }
                return true;
            }
        }else
        {
            sender.sendMessage(messageHandler.formatMessage(ChatColor.RED + "Please enter a valid command!"));
        }
        return true;
    }

    public boolean infoMessage(CommandSender sender)
    {
        sender.sendMessage(ChatColor.DARK_GRAY + "======================");
        sender.sendMessage(ChatColor.AQUA + "/token(s) " + ChatColor.DARK_GRAY  + "opens GUI");
        sender.sendMessage(ChatColor.AQUA + "/token(s) give {player} {amount} " + ChatColor.DARK_GRAY  + "Admin only!");
        sender.sendMessage(ChatColor.AQUA + "/token(s) balance {player} " + ChatColor.DARK_GRAY  + "Admins only!");
        sender.sendMessage(ChatColor.AQUA + "/token(s) take {player} {amount} " + ChatColor.DARK_GRAY  + "Admins only!");
        sender.sendMessage(ChatColor.AQUA + "/token(s) version " + ChatColor.DARK_GRAY  + "displays plugin info");
        sender.sendMessage(ChatColor.DARK_GRAY + "======================");
        return true;
    }
}
