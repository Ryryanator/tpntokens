package net.ryryf2f.tpntokens.econ;

import me.vagdedes.mysql.database.SQL;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

/**
 * a buyable object with the currency
 */
public abstract class ShopItem
{

    private static int id;

    private static String name;
    private ItemStack displayItem;

    private static String column;


    public ShopItem(int id, String name, String column)
    {
        this.id = id;
        this.name = name;
        this.column = column;
    }

    /**
     * Get ID of item
     * @return id
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * Get name of item
     * @return name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Get's display item
     * @return Itemstack of display item, if display item does not exist, returns bedrock ItemStack
     */
    public ItemStack getDisplayItem()
    {
        if (!displayItem.equals(null))
        {
            return displayItem;
        }

        return new ItemStack(Material.BEDROCK,1);
    }

    /**
     * Sets the display item in GUI
     * @param desc - lore for item
     * @param mat - material of item
     */
    public void setDisplayName(ArrayList<String> desc, Material mat)
    {
        ItemStack i = new ItemStack(mat, 1);
        ItemMeta iMeta = i.getItemMeta();
        iMeta.setDisplayName(name);
        iMeta.setLore(desc);
        i.setItemMeta(iMeta);


        this.displayItem = i;
    }

    /**
     * Goes through all requirements to purchase
     * @return true if user does, else false
     */
    public abstract boolean meetsRequirements();

    /**
     * Commands to run on purchase
     * @param player
     */
    public abstract void onPurchase(OfflinePlayer player);


    /**
     * gets the player's item state data from sql server
     * @param uuid player's UUID
     * @return Object
     */
    protected Object getRawSqlData(UUID uuid)
    {
        return SQL.get(column,"uuid","=",uuid.toString(),"tokens");
    }
}
