package net.ryryf2f.tpntokens.econ;


import net.ryryf2f.tpntokens.TpnTokens;
import org.bukkit.OfflinePlayer;

/**
 * Main economy for TpnEconomy, using code from VaultAPI
 * Credit to VaultAPI
 */
public class TpnEconomy
{

    /**
     * Checks if economy method is enabled.
     * @return True if enabled, else false
     */
    public boolean isEnabled()
    {
        return (TpnTokens.getInstance() != null);
    }

    /**
     * Gets name of economy method
     * @return Name of Economy Method
     */
    public String getName()
    {
        return "TpnTokens";
    }


    /**
     * Format amount into a human readable String This provides translation into
     * economy specific formatting to improve consistency between plugins.
     *
     * @param amount to format
     * @return Human readable string describing amount
     */
    public String format(int amount)
    {
        return String.valueOf(amount);
    }

    /**
     * Returns the name of the currency in plural form.
     * If the economy being used does not support currency names then an empty string will be returned.
     *
     * @return name of the currency (plural)
     */
    public String currencyNamePlural()
    {
        return TpnTokens.getConfigHandler().getMainConfig().getNamePlural();
    }


    /**
     * Returns the name of the currency in singular form.
     * If the economy being used does not support currency names then an empty string will be returned.
     *
     * @return name of the currency (singular)
     */
    public String currencyNameSingular()
    {
        return TpnTokens.getConfigHandler().getMainConfig().getNameSingular();
    }


    /**
     * Checks if this player has an account on the server yet
     * This will always return true if the player has joined the server at least once
     * as all major economy plugins auto-generate a player account when the player joins the server
     *
     * @param player to check
     * @return if the player has an account
     */
    public boolean hasAccount(OfflinePlayer player)
    {
       return TpnTokens.getDatabaseManager().databaseTokens.playerExists(player);
    }

    /**
     * Checks if this player has an account on the server yet on the given world
     * This will always return true if the player has joined the server at least once
     * as all major economy plugins auto-generate a player account when the player joins the server
     *
     * @param player to check in the world
     * @param worldName world-specific account
     * @return if the player has an account
     */
    public boolean hasAccount(OfflinePlayer player, String worldName)
    {
        // IMPORTANT NOTE: THIS SHOULD ALWAYS BE THE SAME AS HASACCOUNT, WORLD DOES NOT MATTER FOR THIS PLUGIN
        return hasAccount(player);
    }


    /**
     * Gets balance of a player
     *
     * @param player of the player
     * @return Amount currently held in players account
     */
    public int getBalance(OfflinePlayer player)
    {
        if (hasAccount(player))
        {
            return TpnTokens.getDatabaseManager().databaseTokens.getBalance(player);
        }
        return 0;
    }


    /**
     * Gets balance of a player on the specified world.
     * IMPLEMENTATION SPECIFIC - if an economy plugin does not support this the global balance will be returned.
     * @param player to check
     * @param world name of the world
     * @return Amount currently held in players account
     */
    public int getBalance(OfflinePlayer player, String world)
    {
        // IMPORTATN NOTE: THIS SHOULD ALWAYS BE THE SAME AS GETBALANCE, WORLD DOES NOT MATTER FOR THIS PLUGIN
        return getBalance(player);
    }

    /**
     * Checks if the player account has the amount - DO NOT USE NEGATIVE AMOUNTS
     *
     * @param player to check
     * @param amount to check for
     * @return True if <b>player</b> has <b>amount</b>, False else wise
     */
    public boolean has(OfflinePlayer player, int amount)
    {
        if (hasAccount(player))
        {
            int balance = getBalance(player);

            if (amount <= balance)
            {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the player account has the amount in a given world - DO NOT USE NEGATIVE AMOUNTS
     * IMPLEMENTATION SPECIFIC - if an economy plugin does not support this the global balance will be returned.
     *
     * @param player to check
     * @param worldName to check with
     * @param amount to check for
     * @return True if <b>player</b> has <b>amount</b>, False else wise
     */
    public boolean has(OfflinePlayer player, String worldName, int amount)
    {
        // WORLDS DONT MATTER
        return has(player, amount);
    }

    /**
     * Withdraw an amount from a player - DO NOT USE NEGATIVE AMOUNTS
     *
     * @param player to withdraw from
     * @param amount Amount to withdraw
     * @return Detailed response of transaction
     */
    public boolean withdrawPlayer(OfflinePlayer player, int amount)
    {
        if (!hasAccount(player))
        {
            return false;
        }
        if (!has(player, amount))
        {
            return false;
        }

        TpnTokens.getDatabaseManager().databaseTokens.setBalance(player.getUniqueId(), (getBalance(player) - amount));
        return true;
    }


    /**
     * Withdraw an amount from a player on a given world - DO NOT USE NEGATIVE AMOUNTS
     * IMPLEMENTATION SPECIFIC - if an economy plugin does not support this the global balance will be returned.
     * @param player to withdraw from
     * @param worldName - name of the world
     * @param amount Amount to withdraw
     * @return Detailed response of transaction
     */
    public boolean withdrawPlayer(OfflinePlayer player, String worldName, int amount)
    {
        // WORLDS DO NOT MATTER
        return withdrawPlayer(player, amount);
    }

    /**
     * Deposit an amount to a player - DO NOT USE NEGATIVE AMOUNTS
     *
     * @param player to deposit to
     * @param amount Amount to deposit
     * @return Detailed response of transaction
     */
    public boolean depositPlayer(OfflinePlayer player, int amount)
    {
        if (!hasAccount(player))
        {
            return false;
        }

        TpnTokens.getDatabaseManager().databaseTokens.setBalance(player.getUniqueId(), (getBalance(player) + amount));
        return true;
    }


    /**
     * Deposit an amount to a player - DO NOT USE NEGATIVE AMOUNTS
     * IMPLEMENTATION SPECIFIC - if an economy plugin does not support this the global balance will be returned.
     *
     * @param player to deposit to
     * @param worldName name of the world
     * @param amount Amount to deposit
     * @return Detailed response of transaction
     */
    public boolean depositPlayer(OfflinePlayer player, String worldName, int amount)
    {
        // WORLD TYPE DOESNT MATTER
        return depositPlayer(player,amount);
    }

    /**
     * Directly set an amount to a player - USE THIS SMARTLY
     * @param player to deposit to
     * @param amount amount to set
     * @return
     */
    public boolean setBalancePlayer(OfflinePlayer player, int amount)
    {
        TpnTokens.getDatabaseManager().databaseTokens.setBalance(player, amount);
        return true;
    }

    /**
     * Attempts to create a player account for the given player
     * @param player OfflinePlayer
     * @return if the account creation was successful
     */
    public boolean createPlayerAccount(OfflinePlayer player)
    {
        return TpnTokens.getDatabaseManager().databaseTokens.createAccount(player);
    }


    /**
     * Attempts to create a player account for the given player on the specified world
     * IMPLEMENTATION SPECIFIC - if an economy plugin does not support this then false will always be returned.
     * @param player OfflinePlayer
     * @param worldName String name of the world
     * @return if the account creation was successful
     */
    public boolean createPlayerAccount(OfflinePlayer player, String worldName)
    {
        // WORLD DOES NOT MATTER
        return createPlayerAccount(player);
    }
}
