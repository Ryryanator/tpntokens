package net.ryryf2f.tpntokens.frontpack;

import net.ryryf2f.tpntokens.TpnTokens;
import net.ryryf2f.tpntokens.econ.Account;
import net.ryryf2f.tpntokens.frontpack.items.Item;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.UUID;

public class MainGui
{


    private final Inventory inv;
    private final Account account;

    public MainGui(UUID uuid)
    {
        account = new Account(uuid);
        inv = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Tokens Menu");
        initItems();
    }

    public void initItems()
    {

        ArrayList<String> desc = new ArrayList<>();

        Item tokenAmountItem = new Item(0, ChatColor.AQUA + "Tokens: " + account.economy.getBalance(account.player) + TpnTokens.getConfigHandler().getMainConfig().getSymbol());
        desc.add(ChatColor.GOLD + "This is how many credits you have!");
        desc.add(ChatColor.GOLD + " To get more, go to the token shop!");
        tokenAmountItem.setDisplayInfo(desc, Material.EMERALD);

        desc.clear();

        Item cancelItem = new Item(1, ChatColor.RED + "Press to exit");
        cancelItem.setDisplayInfo(desc, Material.BARRIER);

        Item buyTokenItem = new Item(2, ChatColor.AQUA + "Purchase Tokens");
        desc.add(ChatColor.GOLD + "Click to trade money for tokens!");
        buyTokenItem.setDisplayInfo(desc, Material.DIAMOND);

        desc.clear();

        Item rankItem = new Item(3, ChatColor.AQUA + "Purchase Ranks");
        desc.add(ChatColor.GOLD + "Click to spend your tokens on a new rank!");
        rankItem.setDisplayInfo(desc, Material.CYAN_BANNER);

        desc.clear();
        Item newItem = new Item(4, ChatColor.AQUA + "New rewards coming soon!");
        newItem.setDisplayInfo(desc, Material.FIREWORK_ROCKET);

        inv.setItem(0, tokenAmountItem.getDisplayItem());
        inv.setItem(8, cancelItem.getDisplayItem());
        inv.setItem(2, buyTokenItem.getDisplayItem());
        inv.setItem(5, rankItem.getDisplayItem());
        inv.setItem(6, newItem.getDisplayItem());
    }

    public Inventory get()
    {
        return inv;
    }
}
