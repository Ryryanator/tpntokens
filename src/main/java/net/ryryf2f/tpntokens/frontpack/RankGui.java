package net.ryryf2f.tpntokens.frontpack;


import me.ryryf2f.tpnlib.TpnLib;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.track.Track;
import net.ryryf2f.tpntokens.TpnTokens;
import net.ryryf2f.tpntokens.econ.Account;
import net.ryryf2f.tpntokens.frontpack.items.Item;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class RankGui
{

    private final Inventory inv;
    private final Account account;

    Optional<Track> track;
    Optional<User> user;

    public RankGui(UUID uuid)
    {

        if (TpnTokens.getLuckPerms() == null)
        {
            TpnTokens.getMessageHandler().severe("LUCKPERMS API IS NOT WORKING");
        }

        track = Optional.of(TpnTokens.getLuckPerms().getTrackManager().getTrack("main"));

        inv = Bukkit.createInventory(null, 18, ChatColor.AQUA + "Purchase Ranks Here");

        user = Optional.of(TpnTokens.getLuckPerms().getUserManager().getUser(uuid));

        account = new Account(uuid);

        initInventory();
    }


    //obtained : red stained clay
    //unobtained: yellow stained clay
    //next rank: green stained clay

    public void initInventory()
    {

        int indexOfRank = track.get().getGroups().indexOf(user.get().getPrimaryGroup());

        ArrayList<String> obtainedDesc = new ArrayList<>();
        obtainedDesc.add(ChatColor.RED + "You already obtained this rank!");
        obtainedDesc.add(ChatColor.AQUA + "Rank Name" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "Token Price");

        ArrayList<String> unobtainedDesc = new ArrayList<>();
        unobtainedDesc.add(ChatColor.YELLOW + "Purchase earlier ranks first!");
        unobtainedDesc.add(ChatColor.AQUA + "Rank Name" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "Token Price");

        ArrayList<String> nextrankDesc = new ArrayList<>();
        nextrankDesc.add(ChatColor.GREEN + "Click to rank up!");
        nextrankDesc.add(ChatColor.AQUA + "Rank Name" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "Token Price");


        ArrayList<Item> ranks = new ArrayList<>();
        ranks.add(0, new Item(0, ChatColor.AQUA + "Laborer" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "21"));  //OG 25
        ranks.add(1, new Item(1, ChatColor.AQUA + "Recruit" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "42"));  //OG 50
        ranks.add(2, new Item(2, ChatColor.AQUA + "Novice" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "106"));  //OG 125
        ranks.add(3, new Item(3, ChatColor.AQUA + "Squire" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "242"));  //OG 285
        ranks.add(4, new Item(4, ChatColor.AQUA + "Knight" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "488"));  //OG 575
        ranks.add(5, new Item(5, ChatColor.AQUA + "Sentinel" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "880")); //OG 1035
        ranks.add(6, new Item(6, ChatColor.AQUA + "Captain" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "1462")); //OG 1720
        ranks.add(7, new Item(7, ChatColor.AQUA + "Regent" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "2274")); //OG 2675
        ranks.add(8, new Item(8, ChatColor.AQUA + "Sage" + ChatColor.DARK_GRAY + " : " + ChatColor.GOLD + "3400")); //OG 4000


        if (indexOfRank == track.get().getGroups().size())
        {
            for (Item rank : ranks)
            {
                rank.setDisplayInfo(obtainedDesc, Material.RED_TERRACOTTA);
            }
        }else
        {
            List<String> obtainedRanks = track.get().getGroups().subList(0 , indexOfRank);
            List<String> unobtainedRanks = track.get().getGroups().subList(indexOfRank + 1, track.get().getGroups().size());

            Group nextGroup = TpnTokens.getLuckPerms().getGroupManager().getGroup(track.get().getGroups().get(indexOfRank + 1));
            for (Item rank : ranks)
            {
                for (String obtained : obtainedRanks)
                {
                    //this a check for the default (laborer) rank, this way due to luckperms
                    if (obtained.equalsIgnoreCase("default"))
                    {
                        rank.setDisplayInfo(obtainedDesc, Material.RED_TERRACOTTA);
                    }
                    if (parseRank(rank).equalsIgnoreCase(obtained))
                    {
                        rank.setDisplayInfo(obtainedDesc, Material.RED_TERRACOTTA);
                    }
                }

                for (String unobtained : unobtainedRanks)
                {
                    //this a check for the default (laborer) rank, this way due to luckperms
                    if (unobtained.equalsIgnoreCase("default"))
                    {
                        rank.setDisplayInfo(obtainedDesc, Material.RED_TERRACOTTA);
                    }
                    if (parseRank(rank).equalsIgnoreCase(unobtained))
                    {
                        rank.setDisplayInfo(unobtainedDesc, Material.YELLOW_TERRACOTTA);
                    }
                }
            }

            for (Item rank : ranks)
            {
                if (parseRank(rank).equalsIgnoreCase(nextGroup.getName()))
                {
                    rank.setDisplayInfo(nextrankDesc, Material.GREEN_TERRACOTTA);
                }
            }
        }


        //set all items position
        inv.setItem(0, ranks.get(0).getDisplayItem());
        inv.setItem(1, ranks.get(1).getDisplayItem());
        inv.setItem(2, ranks.get(2).getDisplayItem());
        inv.setItem(3, ranks.get(3).getDisplayItem());
        inv.setItem(4, ranks.get(4).getDisplayItem());
        inv.setItem(5, ranks.get(5).getDisplayItem());
        inv.setItem(6, ranks.get(6).getDisplayItem());
        inv.setItem(7, ranks.get(7).getDisplayItem());
        inv.setItem(8, ranks.get(8).getDisplayItem());

        ArrayList<String> desc = new ArrayList<>();
        Item cancelItem = new Item(2, ChatColor.RED + "Press to exit");
        cancelItem.setDisplayInfo(desc, Material.BARRIER);

        inv.setItem(13, cancelItem.getDisplayItem());

        desc.clear();
        Item tokenAmountItem = new Item(0, ChatColor.AQUA + "Tokens: " + account.economy.getBalance(account.player)  + TpnTokens.getConfigHandler().getMainConfig().getSymbol());
        desc.add(ChatColor.GOLD + "This is how many credits you have!");
        tokenAmountItem.setDisplayInfo(desc, Material.EMERALD);

        inv.setItem(9, tokenAmountItem.getDisplayItem());

    }

    public Inventory get()
    {
        return inv;
    }

    private String parseRank(Item item) //todo make a rank class and have this in it, and make the ranks that way
    {
        return ChatColor.stripColor(item.getName().split(":")[0].replaceAll("\\s", "").toLowerCase());
    }
}