package net.ryryf2f.tpntokens.frontpack.items;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;

public class Item
{

    private int id;
    private String name;
    private ItemStack displayItem;


    public Item(int id, String name)
    {
        this.id = id;
        this.name = name;
       // this.column = column;
    }

    /**
     * Get ID of item
     * @return id
     */
    public int getId()
    {
        return this.id;
    }
    /**
     * Get name of item
     * @return name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Get's display item
     * @return Itemstack of display item, if display item does not exist, returns bedrock ItemStack
     */
    public ItemStack getDisplayItem()
    {
        if (!displayItem.equals(null))
        {
            return displayItem;
        }

        return new ItemStack(Material.BEDROCK,1);
    }

    /**
     * Sets the display item in GUI
     * @param desc - lore for item
     * @param mat - material of item
     */
    public void setDisplayInfo(ArrayList<String> desc, Material mat)
    {
        ItemStack i = new ItemStack(mat, 1);
        ItemMeta iMeta = i.getItemMeta();
        iMeta.setDisplayName(name);
        iMeta.setLore(desc);
        i.setItemMeta(iMeta);

        this.displayItem = i;
    }
}
