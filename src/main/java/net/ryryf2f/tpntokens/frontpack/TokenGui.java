package net.ryryf2f.tpntokens.frontpack;

import net.ryryf2f.tpntokens.TpnTokens;
import net.ryryf2f.tpntokens.econ.Account;
import net.ryryf2f.tpntokens.frontpack.items.Item;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;


import java.util.ArrayList;
import java.util.UUID;

public class TokenGui
{

    private final Inventory inv;
    private final Account account;

    public TokenGui(UUID uuid)
    {
        account = new Account(uuid);
        inv = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Purchase Tokens");
        initItems();
    }

    public void initItems()
    {

        ArrayList<String> desc = new ArrayList<>();

        Item tokenAmountItem = new Item(0, ChatColor.AQUA + "Tokens: " + account.economy.getBalance(account.player) + TpnTokens.getConfigHandler().getMainConfig().getSymbol());
        desc.add(ChatColor.GOLD + "This is how many credits you have!");
        tokenAmountItem.setDisplayInfo(desc, Material.EMERALD);

        desc.clear();

        Item moneyAmountItem = new Item(1, ChatColor.AQUA + "In Game Currency: $" + Math.floor(TpnTokens.getVaultEconomy().getBalance(account.player)));
        desc.add(ChatColor.GOLD + "This is how much in game money you have!");
        moneyAmountItem.setDisplayInfo(desc, Material.GOLD_INGOT);

        desc.clear();

        Item cancelItem = new Item(2, ChatColor.RED + "Press to exit");
        cancelItem.setDisplayInfo(desc, Material.BARRIER);

        Item buyTokenOneItem = new Item(3, ChatColor.AQUA + "Purchase 1 Tokens");
        desc.add(ChatColor.GOLD + "Exchange Rate: $100 : 1 Token");
        buyTokenOneItem.setDisplayInfo(desc, Material.GOLD_NUGGET);

        desc.clear();

        Item buyTokenTwoItem = new Item(4, ChatColor.AQUA + "Purchase 10 Tokens");
        desc.add(ChatColor.GOLD + "Exchange Rate: $100 : 1 Token");
        buyTokenTwoItem.setDisplayInfo(desc, Material.GOLD_NUGGET);

        desc.clear();

        Item buyTokenThreeItem = new Item(5, ChatColor.AQUA + "Purchase 100 Tokens");
        desc.add(ChatColor.GOLD + "Exchange Rate: $100 : 1 Token");
        buyTokenThreeItem.setDisplayInfo(desc, Material.GOLD_NUGGET);


        desc.clear();

        inv.setItem(0, tokenAmountItem.getDisplayItem());
        inv.setItem(8, cancelItem.getDisplayItem());
        inv.setItem(3, buyTokenOneItem.getDisplayItem());
        inv.setItem(4, buyTokenTwoItem.getDisplayItem());
        inv.setItem(5, buyTokenThreeItem.getDisplayItem());

        inv.setItem(1, moneyAmountItem.getDisplayItem());
    }

    public Inventory get()
    {
        return inv;
    }
}
