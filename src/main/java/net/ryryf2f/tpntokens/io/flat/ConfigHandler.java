package net.ryryf2f.tpntokens.io.flat;

import net.ryryf2f.tpntokens.TpnTokens;

public class ConfigHandler
{

    private static MainConfig mainConfig;

    public void initConfigs()
    {
        TpnTokens.getInstance().getDataFolder().mkdir();
        mainConfig = new MainConfig();
    }

    public void shutdownConfigs()
    {
        mainConfig.load();
        mainConfig.flush(true);
    }

    public static MainConfig getMainConfig()
    {
        return mainConfig;
    }


}
