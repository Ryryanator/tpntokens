package net.ryryf2f.tpntokens.io.flat;

import me.ryryf2f.tpnlib.api.io.file.YamlFile;
import net.ryryf2f.tpntokens.TpnTokens;

public class MainConfig extends YamlFile {

    public MainConfig() {
        this.hasDefault = true;
        this.directoryParent = TpnTokens.getInstance().getDataFolder();
        this.directoryChild = "/config.yml";

        createFile();
        load();
    }

    public String getNameSingular()
    {
        return fileConfiguration.getString("token-name-singular");
    }

    public String getNamePlural()
    {
        return fileConfiguration.getString("token-name-plural");
    }

    public String getSymbol()
    {
        return fileConfiguration.getString("token-symbol");
    }

}
