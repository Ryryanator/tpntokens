package net.ryryf2f.tpntokens.io.sql;

import me.vagdedes.mysql.database.SQL;
import net.ryryf2f.tpntokens.TpnTokens;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class DatabaseTokens
{



    public DatabaseTokens()
    {
        TpnTokens.getMessageHandler().info("Starting database...");
        if (!SQL.exists("uuid", "system_var","tokens"))
        {
            setup();
        }

        if (exists())
        {
            TpnTokens.getMessageHandler().info("Database running.");
        }else
        {
            TpnTokens.getMessageHandler().severe("Unable to start tokens database, disabling plugin...");
            TpnTokens.getInstance().getServer().getPluginManager().disablePlugin(TpnTokens.getInstance());
        }

    }

    private void setup()
    {
        SQL.createTable("tokens", "uuid CHAR(36), balance INT(255)");
        SQL.insertData("uuid, balance", "'system_var', '0'", "tokens");
    }

    public boolean playerExists(UUID uuid)
    {
        return SQL.exists("uuid", uuid.toString(), "tokens");
    }

    public boolean playerExists(OfflinePlayer offlinePlayer)
    {
        return playerExists(offlinePlayer.getUniqueId());
    }

    public int getBalance(UUID uuid)
    {
        String s = SQL.get("balance", "uuid", "=", uuid.toString(),"tokens").toString();
        return Integer.parseInt(s);
    }

    public int getBalance(OfflinePlayer offlinePlayer)
    {
       return getBalance(offlinePlayer.getUniqueId());
    }

    public void setBalance(UUID uuid, int amount)
    {
        SQL.set("balance", String.valueOf(amount), "uuid", "=", uuid.toString(),"tokens");
    }

    public void setBalance(OfflinePlayer offlinePlayer, int amount)
    {
        setBalance(offlinePlayer.getUniqueId(), amount);
    }

    public boolean createAccount(UUID uuid)
    {
        try
        {
            SQL.insertData("uuid, balance", "'" + uuid + "', '0'", "tokens");

        }catch(Exception e)
        {
            return false;
        }

        return true;
    }

    public boolean createAccount(OfflinePlayer player)
    {
       return createAccount(player.getUniqueId());
    }

    public boolean exists()
    {
        if (SQL.exists("uuid", "system_var","tokens"))
        {
            return true;
        }
            return false;
    }
}
