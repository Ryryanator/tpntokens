package net.ryryf2f.tpntokens.io.sql;

import me.vagdedes.mysql.basic.Config;
import me.vagdedes.mysql.database.MySQL;
import net.ryryf2f.tpntokens.TpnTokens;

/**
 * Manager for MySQL Database
 * THIS PLUGIN DOES NOT USE TPNLIB SQLLITE
 */
public class DatabaseManager
{

    public static DatabaseTokens databaseTokens;

    public DatabaseManager()
    {
        if (MySQL.isConnected())
        {
            databaseTokens = new DatabaseTokens();
        }else
        {
            TpnTokens.getMessageHandler().severe("NO MYSQL SERVER FOUND, DISABLING PLUGIN");
            TpnTokens.getInstance().getServer().getPluginManager().disablePlugin(TpnTokens.getInstance());
        }
    }

    private void reconnect()
    {
        if (!MySQL.isConnected())
        {
            MySQL.connect();
        }else
        {
            MySQL.reconnect();
        }
    }



}
