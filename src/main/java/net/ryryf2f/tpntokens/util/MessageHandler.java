package net.ryryf2f.tpntokens.util;

import me.ryryf2f.tpnlib.api.util.LogHandler;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

public class MessageHandler extends LogHandler
{
    private static String prefix = ChatColor.GRAY + "[" + ChatColor.AQUA + "Tokens" + ChatColor.GRAY + "]" + ChatColor.RESET;


    public MessageHandler(Plugin plugin)
    {
        super(plugin);
    }


    /**
     * Combines prefix with message
     * @param msg
     * @return String formatted message
     */
    public String formatMessage(String msg)
    {
        return prefix + " " + msg;
    }

    /**
     * Get prefix
     * @return String prefix
     */
    public String getPrefix()
    {
        return prefix;
    }


}
