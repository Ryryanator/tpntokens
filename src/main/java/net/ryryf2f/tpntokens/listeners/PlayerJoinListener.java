package net.ryryf2f.tpntokens.listeners;

import net.ryryf2f.tpntokens.TpnTokens;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener
{

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoinEvent(PlayerJoinEvent event)
    {
        if (!TpnTokens.getEconomy().hasAccount(Bukkit.getOfflinePlayer(event.getPlayer().getUniqueId())))
        {
            TpnTokens.getEconomy().createPlayerAccount(Bukkit.getOfflinePlayer(event.getPlayer().getUniqueId()));
        }
    }
}
