package net.ryryf2f.tpntokens.listeners;

import net.luckperms.api.context.ImmutableContextSet;
import net.ryryf2f.tpntokens.TpnTokens;
import net.ryryf2f.tpntokens.frontpack.MainGui;
import net.ryryf2f.tpntokens.frontpack.RankGui;
import net.ryryf2f.tpntokens.frontpack.TokenGui;
import net.ryryf2f.tpntokens.util.MessageHandler;
import org.bukkit.*;
import org.bukkit.Particle.DustOptions;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.meta.FireworkMeta;

public class InventoryListener implements Listener
{

    MessageHandler messageHandler = TpnTokens.getMessageHandler();

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClickEvent(InventoryClickEvent event)
    {
        if (event.getView().getTitle().equals(ChatColor.AQUA + "Tokens Menu"))
        {
            mainGuiHandler(event);
        }else if (event.getView().getTitle().equals(ChatColor.AQUA + "Purchase Ranks Here"))
        {
            rankGuiHandler(event);
        }else if (event.getView().getTitle().equals(ChatColor.AQUA + "Purchase Tokens"))
        {
            tokenGuiHandler(event);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryDragEvent(InventoryDragEvent event)
    {
        if (event.getView().getTitle().equals(ChatColor.AQUA + "Tokens Menu") || event.getView().getTitle().equals(ChatColor.AQUA + "Purchase Ranks Here") || event.getView().getTitle().equals(ChatColor.AQUA + "Purchase Tokens"))
        {
            event.setCancelled(true);
        }
    }



    public void mainGuiHandler(InventoryClickEvent event)
    {
        if(event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta())
        {
            event.setCancelled(true);
        }else if (event.getCurrentItem().getType().equals(Material.BARRIER))
        {
            event.setCancelled(true);
            event.getWhoClicked().closeInventory();
        }else
        {
            if (event.getCurrentItem().getType().equals(Material.EMERALD) || event.getCurrentItem().getType().equals(Material.FIREWORK_ROCKET))
            {
                event.setCancelled(true);
            }else
            {
                if (event.getCurrentItem().getType().equals(Material.DIAMOND))
                {
                    event.setCancelled(true);
                    TokenGui gui = new TokenGui(((Player) event.getWhoClicked()).getPlayer().getUniqueId());
                    event.getWhoClicked().openInventory(gui.get());
                }else
                {
                    if (event.getCurrentItem().getType().equals(Material.CYAN_BANNER))
                    {
                        event.setCancelled(true);
                        if (event.getWhoClicked() instanceof Player)
                        {
                            Player player = ((Player) event.getWhoClicked()).getPlayer();
                            RankGui gui = new RankGui(((Player) event.getWhoClicked()).getPlayer().getUniqueId());
                            event.getWhoClicked().openInventory(gui.get());
                        }
                    }
                }
            }
        }


    }

    public void rankGuiHandler(InventoryClickEvent event)
    {
        if(event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta() || event.getCurrentItem().getType().equals(Material.EMERALD))
        {
            event.setCancelled(true);
        }else if (event.getCurrentItem().getType().equals(Material.BARRIER))
        {
            event.setCancelled(true);
            MainGui mainGui = new MainGui(event.getWhoClicked().getUniqueId());
            event.getWhoClicked().openInventory(mainGui.get());
        }else if (event.getCurrentItem().getType().equals(Material.RED_TERRACOTTA) || event.getCurrentItem().getType().equals(Material.YELLOW_TERRACOTTA))
        {
            event.setCancelled(true);
        }else if (event.getCurrentItem().getType().equals(Material.GREEN_TERRACOTTA))
        {
            event.setCancelled(true);
            int price = Integer.parseInt(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).split(":")[1].replaceAll("\\s", "").toLowerCase());

            if (TpnTokens.getEconomy().has(Bukkit.getOfflinePlayer(event.getWhoClicked().getUniqueId()), price))
            {
                TpnTokens.getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(event.getWhoClicked().getUniqueId()), price);

                TpnTokens.getLuckPerms().getTrackManager().getTrack("main").promote(TpnTokens.getLuckPerms().getUserManager().getUser(event.getWhoClicked().getUniqueId()), ImmutableContextSet.empty());
                TpnTokens.getLuckPerms().getUserManager().saveUser(TpnTokens.getLuckPerms().getUserManager().getUser(event.getWhoClicked().getUniqueId()));


                event.getWhoClicked().closeInventory();
                event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.GOLD + "Congratulations on ranking up!"));

                rankUpFun((Player)event.getWhoClicked());

                Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.DARK_AQUA + ChatColor.BOLD + "Announcements" + ChatColor.RESET + ChatColor.GRAY + "] " + ChatColor.GOLD + "Congratulations to " + event.getWhoClicked().getName() + " on ranking up to " + TpnTokens.getLuckPerms().getUserManager().getUser(event.getWhoClicked().getUniqueId()).getPrimaryGroup() + "!");

            }else
            {
                event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.RED + "You do not have enough tokens!"));
            }
        }
    }

    public void tokenGuiHandler(InventoryClickEvent event)
    {

        if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta())
        {
            event.setCancelled(true);
        }else if(event.getCurrentItem().getType().equals(Material.BARRIER))
        {
            event.setCancelled(true);
            MainGui mainGui = new MainGui(event.getWhoClicked().getUniqueId());
            event.getWhoClicked().openInventory(mainGui.get());
        } else if (event.getCurrentItem().getType().equals(Material.EMERALD) || event.getCurrentItem().getType().equals(Material.GOLD_INGOT))
        {
            event.setCancelled(true);
        }else if (event.getCurrentItem().getType().equals(Material.GOLD_NUGGET))
        {
            event.setCancelled(true);

            OfflinePlayer player = (OfflinePlayer) event.getWhoClicked();

            int tokenNumber = Integer.parseInt(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).replaceAll("[\\D]",""));

            if (tokenNumber == 1)
            {
                if (TpnTokens.getVaultEconomy().has(player, 100))
                {
                    TpnTokens.getVaultEconomy().withdrawPlayer(player, 100);
                    TpnTokens.getEconomy().depositPlayer(player, 1);
                    event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "You've purchased 1 token!"));
                    event.getWhoClicked().closeInventory();
                    TokenGui gui = new TokenGui(player.getUniqueId());
                    event.getWhoClicked().openInventory(gui.get());
                }else
                {
                    event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.RED + "You do not have enough money!"));
                }
            }

            if (tokenNumber == 10)
            {
                if (TpnTokens.getVaultEconomy().has(player, 1000))
                {
                    TpnTokens.getVaultEconomy().withdrawPlayer(player, 1000);
                    TpnTokens.getEconomy().depositPlayer(player, 10);
                    event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "You've purchased 10 token!"));
                    event.getWhoClicked().closeInventory();
                    TokenGui gui = new TokenGui(player.getUniqueId());
                    event.getWhoClicked().openInventory(gui.get());
                }else
                {
                    event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.RED + "You do not have enough money!"));
                }
            }

            if (tokenNumber == 100)
            {
                if (TpnTokens.getVaultEconomy().has(player, 10000))
                {
                    TpnTokens.getVaultEconomy().withdrawPlayer(player, 10000);
                    TpnTokens.getEconomy().depositPlayer(player, 100);
                    event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.DARK_GRAY + "You've purchased 100 token!"));
                    event.getWhoClicked().closeInventory();
                    TokenGui gui = new TokenGui(player.getUniqueId());
                    event.getWhoClicked().openInventory(gui.get());
                }else
                {
                    event.getWhoClicked().sendMessage(messageHandler.formatMessage(ChatColor.RED + "You do not have enough money!"));
                }
            }
        }
    }


    public static void rankUpFun(Player player)
    {
        DustOptions dustOptions1 = new DustOptions(Color.fromRGB(32, 122, 140), 10); //somewhat aqua
        DustOptions dustOptions2 = new DustOptions(Color.fromRGB(76, 76, 77), 10); //somewhat gray

        player.spawnParticle(Particle.REDSTONE, player.getLocation(), 50, dustOptions1);
        player.spawnParticle(Particle.REDSTONE, player.getLocation(), 50, dustOptions2);


        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, .5F, 1.0F);
        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_XYLOPHONE, .5F, 5.0F);
    }


    public static void spawnFireworks(Location location, int amount) {
        Location loc = location;
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(2);
        fwm.addEffect(FireworkEffect.builder().withColor(Color.LIME).flicker(true).build());

        fw.setFireworkMeta(fwm);
        fw.detonate();

        for (int i = 0; i < amount; i++) {
            Firework fw2 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            fw2.setFireworkMeta(fwm);
        }
    }

}
